import os
import subprocess
import string
import json
import datetime
import dateutil.parser
import pytz


MIXIN = 'mixin.json'
CHO_DIR = 'cho'


class ChoSheet:

  def __init__(self, fn, pdf_fn=None):
    if fn.endswith('.cho'):
      self.cho_fn = fn
      self.base_fn = fn[0:-4]
    else:
      self.base_fn = fn
      self.cho_fn = fn + '.cho'
    if pdf_fn:
      self.pdf_fn = pdf_fn
    else:
      self.pdf_fn = self.base_fn + '.pdf'

  def __str__(self):
    return '[%s], cho=%s, pdf=%s' % (self.base_fn, self.cho_fn, self.pdf_fn)

  def cho_mtime(self):
    return datetime.datetime.fromtimestamp(os.lstat(self.cho_fn).st_mtime, tz=pytz.utc).astimezone(pytz.timezone('Europe/Berlin'))

  def cho_last_git_commit(self):
    cmd = ['git', '--no-pager', 'log', '-1', '--format=%cd', '--date=iso', self.cho_fn]
    try:
      raw = subprocess.check_output(cmd).strip()
      return dateutil.parser.parse(raw)
    except:
      return None

  def cho_timestamp(self):
    t1 = self.cho_mtime()
    t2 = self.cho_last_git_commit()
    if not t2:
      return t1
    return min(t1, t2)

  def is_pdf_current(self):
    try:
      pdf_mtime = os.lstat(self.pdf_fn).st_mtime
    except FileNotFoundError:
      return False
    cho_mtime = os.lstat(self.cho_fn).st_mtime
    return pdf_mtime >= cho_mtime

  def render_pdf(self, chordpro_conf=None, footer=['', '', '']):
    print('processing %s -> %s' % (self.cho_fn, self.pdf_fn))
    config_snippet = {'pdf': {'formats': {'first': {'footer': footer}}}}
    with open(MIXIN, 'w') as f:
      f.write(json.dumps(config_snippet))

    cmd = ['chordpro']
    if chordpro_conf:
      cmd.append('--config='+chordpro_conf)
    cmd.extend(('--config='+MIXIN, '--diagrams=none', '-o', self.pdf_fn, self.cho_fn))
    try:
      subprocess.check_output(cmd, stderr=subprocess.PIPE)
      return self.pdf_fn
    except subprocess.CalledProcessError as e:
      print('ERROR: chordpro returned %d' % e.returncode)
      print(e.stderr.decode())
      raise
    finally:
      os.unlink(MIXIN)


def files():
  c = list()
  for e in os.listdir(CHO_DIR):
    if e.endswith('.cho') and e != 'template.cho':
      c.append(os.path.join(CHO_DIR, os.path.splitext(e)[0]))
  return sorted(c)
