# Jambook - an open songbook for jamming

All the song files are in subdirectory cho.

They are in [Chordpro format](https://chordpro.org/chordpro/chordpro-file-format-specification/).

**run_chordpro** is a wrapper around [Chordpro](https://www.chordpro.org/). It renders the CHO files into PDFs, in A4, one page in one file per song.

**combine_pdfs** is a wrapper around [xelatex](http://xetex.sourceforge.net/), [pdfunite](https://poppler.freedesktop.org/) and [pdfjam](https://github.com/DavidFirth/pdfjam). It produces the three output files:

* songbook_a4.pdf
* songbook_a5.pdf
* songbook_a5book.pdf - ready to be printed by e.g. [PinguinDruck](https://pinguindruck.de/shop/category/broschueren).

# Installation on Ubuntu

Tested on:

* [Ubuntu Xenial (16.04)](https://releases.ubuntu.com/16.04/)
* [Ubuntu Artful (18.04)](https://releases.ubuntu.com/18.04/)

**run_chordpro** needs these packages, install as root:
```
apt install make gcc python3-pip
perl -MCPAN -e "CPAN::Shell->notest('install', 'chordpro')"
pip3 install --user -r requirements.txt
```
For **combine_pdfs** you will need these additional packages:
```
apt install texlive-xetex texlive-extra-utils poppler-utils
```
